import angular from 'angular';

const app = angular.module('app', []);

app.component('helloWorld', {
    bindings: {
        message: '=',
    },
    template: '<div>Hello {{$ctrl.message}}</div>',
    controller: function () {
        this.message = 'World';
    },
});
