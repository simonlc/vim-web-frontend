if !exists("main_syntax")
  if version < 600
    syntax clear
  elseif exists("b:current_syntax")
    finish
  endif
  let main_syntax = 'fedjs'
endif

" Dollar sign is permitted anywhere in an identifier
" if v:version > 704 || v:version == 704 && has('patch1142')
"   syntax iskeyword @,48-57,_,192-255,$
" else
"   setlocal iskeyword+=$
" endif

" ==============================================================================
" Comments
" ==============================================================================
syn keyword fedjsCommentTodo     TODO contained
syn keyword fedjsCommentFixme    FIXME contained
syn keyword fedjsCommentXXX      XXX contained
syn keyword fedjsCommentTBD      TBD contained
syn match   fedjsLineComment     "\/\/.*" contains=@Spell,fedjsCommentTodo
syn match   fedjsCommentSkip     "^[ \t]*\*\($\|[ \t]\+\)"
syn region  fedjsComment         start="/\*"  end="\*/" contains=@Spell,fedjsCommentTodo
" ==============================================================================
" Modules
" ==============================================================================

" TODO Find out if we need a space in between import and a string
" TODO Check if semi colon insertion makes a semi colon on a new line 'obsolete'
" TODO + seems to be a greedy match for strings
" TODO Make more robust checking in between from and import (is there value in
" this or performance considerations?)
" syn match fedjsImportDeclaration     "import\_[A-z1-9${} *,]\{-}from\_s\{-}\".\+\"\_s\{};\?" contains=fedjsFrom
" syn match fedjsImportDeclaration     "import\_[A-z1-9${} *,]\{-}from\_s\{-}'.\+'\_s\{};\?" contains=fedjsFrom
" syn match fedjsImportDeclaration     "import\_s\{-}\".\+\"\_s\{};\?"
" syn match fedjsImportDeclaration     "import\_s\{-}'.\+'\_s\{};\?"
syn match fedjsImportDeclaration     "import\_[A-z1-9$_{} *,]\{-}from\_s\{-}\".\+\"\_s\{};\?"
syn match fedjsImportDeclaration     "import\_[A-z1-9$_{} *,]\{-}from\_s\{-}'.\+'\_s\{};\?"
syn match fedjsImportDeclaration     "import\_s\{-}\".\+\"\_s\{};\?" contains=fedjsImport,fedjsString
syn match fedjsImportDeclaration     "import\_s\{-}'.\+'\_s\{};\?" contains=fedjsImport,fedjsString

" syn keyword fedjsImport              contained import skipwhite skipempty nextgroup=fedjsString
" TODO nextgroup importclause or string
syn keyword fedjsImport              contained import skipwhite skipempty
syn keyword fedjsFrom                contained from skipwhite skipempty nextgroup=fedjsString

syn match fedjsModuleAsterisk        contained /\*/ skipwhite skipempty nextgroup=fedjsModuleAs
syn keyword fedjsModuleAs            contained as skipwhite skipempty nextgroup=fedjsModuleKeyword,fedjsExportDefaultGroup

syn keyword fedjsExport             export skipwhite skipempty nextgroup=@fedjsAll,fedjsModuleGroup,fedjsExportDefault,fedjsModuleAsterisk,fedjsModuleKeyword
syn match   fedjsModuleKeyword      contained /\k\+/ skipwhite skipempty nextgroup=fedjsModuleAs,fedjsFrom,fedjsModuleComma
syn keyword fedjsExportDefault      contained default skipwhite skipempty nextgroup=@fedjsExpression
syn keyword fedjsExportDefaultGroup contained default skipwhite skipempty nextgroup=fedjsModuleAs,fedjsFrom,fedjsModuleComma
syn match   fedjsModuleAsterisk     contained /\*/ skipwhite skipempty nextgroup=fedjsModuleKeyword,fedjsModuleAs,fedjsFrom
" syn keyword fedjsFrom               contained from skipwhite skipempty nextgroup=fedjsString
syn match   fedjsModuleComma        contained /,/ skipwhite skipempty nextgroup=fedjsModuleKeyword,fedjsModuleAsterisk,fedjsModuleGroup

" hi fedjsImport guibg=Blue
hi fedjsImportDeclaration guibg=blue
hi fedjsString guibg=green
hi fedjsImport guibg=red
" hi fedjsModuleAsterisk guibg=Green
" hi fedjsFrom guibg=Red

syn match   fedjsSpecial         "\\\d\d\d\|\\."
" syn region  fedjsStringD         start=+"+  skip=+\\\\\|\\"+  end=+"\|$+  contains=fedjsSpecial,@htmlPreproc
" syn region  fedjsStringS         start=+'+  skip=+\\\\\|\\'+  end=+'\|$+  contains=fedjsSpecial,@htmlPreproc

" from vim-javascript
syn region  fedjsString           start=+"+  skip=+\\\("\|$\)+  end=+"\|$+  contains=jsSpecial,@Spell extend
syn region  fedjsString           start=+'+  skip=+\\\('\|$\)+  end=+'\|$+  contains=jsSpecial,@Spell extend

syn match   fedjsSpecialCharacter "'\\.'"
syn match   fedjsNumber         "-\=\<\d\+L\=\>\|0[xX][0-9a-fA-F]\+\>"
syn region  fedjsRegexpString     start=+/[^/*]+me=e-1 skip=+\\\\\|\\/+ end=+/[gim]\{0,2\}\s*$+ end=+/[gim]\{0,2\}\s*[;.,)\]}]+me=e-1 contains=@htmlPreproc oneline

syn keyword fedjsConditional if else switch
syn keyword fedjsRepeat    while for do in
syn keyword fedjsBranch    break continue
syn keyword fedjsOperator    new delete instanceof typeof
syn keyword fedjsType    Array Boolean Date Function Number Object String RegExp
syn keyword fedjsStatement   return with
syn keyword fedjsBoolean   true false
syn keyword fedjsNull    null undefined
syn keyword fedjsIdentifier  arguments this var let
syn keyword fedjsLabel   case default
syn keyword fedjsException   try catch finally throw
syn keyword fedjsMessage   alert confirm prompt status
syn keyword fedjsGlobal    self window top parent
syn keyword fedjsMember    document event location
syn keyword fedjsDeprecated  escape unescape
" syn keyword fedjsReserved    abstract boolean byte char class const debugger double enum export extends final float goto implements import int interface long native package private protected public short static super synchronized throws transient volatile

if exists("javaScript_fold")
  syn match fedjsFunction  "\<function\>"
  syn region  fedjsFunctionFold  start="\<function\>.*[^};]$" end="^\z1}.*$" transparent fold keepend

  syn sync match fedjsSync grouphere fedjsFunctionFold "\<function\>"
  syn sync match fedjsSync grouphere NONE "^}"

  setlocal foldmethod=syntax
  setlocal foldtext=getline(v:foldstart)
else
  syn keyword fedjsFunction  function
  syn match fedjsBraces     "[{}\[\]]"
  syn match fedjsParens     "[()]"
endif

syn sync fromstart
syn sync maxlines=100

if main_syntax == "fedjs"
  syn sync ccomment fedjsComment
endif

" Define the default highlighting.
" Only when an item doesn't have highlighting yet
" hi def link fedjsComment   Comment
" hi def link fedjsLineComment   Comment
" hi def link fedjsCommentTodo   Todo
" hi def link fedjsSpecial   Special
" hi def link fedjsStringS   String
" hi def link fedjsStringD   String
" hi def link fedjsCharacter   Character
" hi def link fedjsSpecialCharacter  fedjsSpecial
" hi def link fedjsNumber    fedjsValue
" hi def link fedjsConditional   Conditional
" hi def link fedjsRepeat    Repeat
" hi def link fedjsBranch    Conditional
" hi def link fedjsOperator    Operator
" hi def link fedjsType      Type
" hi def link fedjsStatement   Statement
" hi def link fedjsFunction    Function
" hi def link fedjsBraces    Function
" hi def link fedjsError   Error
" hi def link javaScrParenError   fedjsError
" hi def link fedjsNull      Keyword
" hi def link fedjsBoolean   Boolean
" hi def link fedjsRegexpString    String

" hi def link fedjsIdentifier    Identifier
" hi def link fedjsLabel   Label
" hi def link fedjsException   Exception
" hi def link fedjsMessage   Keyword
" hi def link fedjsGlobal    Keyword
" hi def link fedjsMember    Keyword
" hi def link fedjsDeprecated    Exception
" " hi def link fedjsReserved    Keyword
" hi def link fedjsDebug   Debug
" hi def link fedjsConstant    Label
