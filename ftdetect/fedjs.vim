au BufNewFile,BufRead *.js set filetype=fedjs
au BufNewFile,BufRead *.jsm set filetype=fedjs
au BufNewFile,BufRead Jakefile set filetype=fedjs
au BufNewFile,BufRead *.es6 set filetype=fedjs

fun! s:SelectJavascript()
  if getline(1) =~# '^#!.*/bin/\%(env\s\+\)\?node\>'
    set ft=fedjs
  endif
endfun
au BufNewFile,BufRead * call s:SelectJavascript()
